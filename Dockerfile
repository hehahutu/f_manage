FROM tiangolo/uwsgi-nginx:python3.6

ENV DEFAULT_ENV UNITTEST
ENV DB_URL mysql+pymysql://root:meng@39.105.167.167/s_manage?charset=utf8mb4
ENV SECRET_KEY !0e8$7pya^3^0f)j-v2pybi804-4ufsnrz09icjze7qkd7%=@f
ENV PYTHONPATH=/app

COPY . /app
RUN pip install -r /app/requirements.txt

WORKDIR /app