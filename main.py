"""
CREAT: 2017/12/9
AUTHOR:　HEHAHUTU
"""
from flask import jsonify
from app import create_app

app = create_app()


@app.errorhandler(404)
def page_not_found(e):
    # snip
    return jsonify({'code': 404, 'message': 'url不存在'}), 404


if __name__ == '__main__':
    app.run()
