"""
Author: Meng
Date: 2019/1/13
"""
from app.models.cloud.folder import DiskFolder, DiskFile
from flask_restful import Resource, reqparse, fields
from app.api import api
from f_lib.principal.auth import auth_token
from f_lib.principal.safety import safety_strategy
from f_lib.code import Msg
from f_lib.marshal_list import marshal
from flask import g


file_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'group_id': fields.Integer,
    'folder_path': fields.String,
    'create_time': fields.datetime,
    'type': fields.String,
    'file_size': fields.Integer,
    'file_name': fields.String,
    'is_thumb': fields.Integer,
    'is_share': fields.Integer
}


class TrashListPath(Resource):
    folder_parse = reqparse.RequestParser()
    folder_parse.add_argument('folder_path', required=True, type=str)
    field_folder = dict(
        id=int,
        floder_name=str,

    )

    @safety_strategy
    @auth_token()
    def post(self):
        user_id = g.user.id
        file = DiskFile.query_many(is_topic=None, user_id=user_id, is_trash=1)
        mi = marshal(file_fields, file)
        paths = mi
        return Msg.success(msg='ok', paths=paths)


class TrashRestore(Resource):
    folder_parse = reqparse.RequestParser()
    folder_parse.add_argument('id', required=True, type=int)

    @safety_strategy
    @auth_token()
    def post(self):
        arg = self.folder_parse.parse_args()
        user_id = g.user.id
        file = DiskFile.query_one(is_topic=None, user_id=user_id, is_trash=1, id=arg['id'])
        if file:
            file.update(is_trash=0)
            return Msg.success()

        return Msg.failed_dict(1002)


class TrashDel(Resource):
    folder_parse = reqparse.RequestParser()
    folder_parse.add_argument('id', required=True, type=int)

    @safety_strategy
    @auth_token()
    def post(self):
        arg = self.folder_parse.parse_args()
        user_id = g.user.id
        file = DiskFile.query_one(is_topic=None, user_id=user_id, is_trash=1, id=arg['id'])
        if file:
            file.delete()
            return Msg.success()

        return Msg.failed_dict(1002)


api.add_resource(TrashListPath, '/path/trash', endpoint='trash_list')
api.add_resource(TrashRestore, '/path/trash/restore', endpoint='trash_restore')
api.add_resource(TrashDel, '/path/trash/del', endpoint='trash_del')