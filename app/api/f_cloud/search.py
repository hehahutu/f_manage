"""
Author: Meng
Date: 2018/7/18
"""

from app.models.cloud.folder import DiskFile
from flask_restful import Resource, reqparse, fields
from app.api import api
from f_lib.principal.auth import auth_token
from werkzeug.datastructures import FileStorage
from f_lib.code import Msg
from flask import g, make_response
from settings import UPLOAD_PATH
import os
from f_lib.celerys.backup_file import back_up_task
import urllib.parse


file_fields = {

}


class SearchFiles(Resource):
    parse = reqparse.RequestParser()
    parse.add_argument('keyword', type=str, required=True)

    @auth_token()
    def get(self):
        arg = self.parse.parse_args()
        # print(arg)
        all_list = ['id', 'name', 'file_name', 'create_time', 'file_size']
        files = DiskFile.query_search_file_like(keyword=arg.get('keyword'), user_id=g.user.id, check=all_list)

        path = []
        if files:
            files = list(files)
            for item in files:
                file_fields['id'] = item[0]
                file_fields['name'] = item[1]
                file_fields['file_name'] = item[2]
                file_fields['create_time'] = str(item[3])
                file_fields['file_size'] = item[4]
                file_fields['type'] = 'file'
                path.append(file_fields.copy())
        return Msg.success(path=path, total=len(path))


api.add_resource(SearchFiles, '/search', endpoint='search')