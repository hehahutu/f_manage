"""
Author: Meng
Date: 2018/11/25
"""

from flask_restful import Resource, reqparse
from app.api import api
from flask import request, make_response
import requests


class Pyc(Resource):
    def get(self):
        try:
            buildDate = request.args.get('buildDate')
            buildNumber = request.args.get('buildNumber')
            clientVersion = request.args.get('clientVersion')
            hostName = request.args.get('hostName')
            machineId = request.args.get('machineId')
            productCode = request.args.get('productCode')
            productFamilyId = request.args.get('productFamilyId')
            salt = request.args.get('salt')
            secure = request.args.get('secure')
            userName = request.args.get('userName', 'admin')
            # userName = 'hehahutu'
            version = request.args.get('version')
            versionNumber = request.args.get('versionNumber')
            url = f'http://127.0.0.1:1027/rpc/obtainTicket.action?buildDate={buildDate}&buildNumber={buildNumber}&clientVersion={clientVersion}&hostName={hostName}&machineId={machineId}&productCode={productCode}&productFamilyId={productFamilyId}&salt={salt}&secure={secure}&userName={userName}&version={version}&versionNumber={versionNumber}'

            # print(url)
            html = requests.get(url)
            # data = str(html.content).replace('ilanyu', 'hehahutu')
            resp = make_response(html.content)
            # resp.headers = html.headers
            resp.headers['Content-Type'] = 'text/plain; charset=utf-8'
            resp.headers['Server'] = 'fasthttp'
            return resp
        except Exception as e:
            return str(e)


api.add_resource(Pyc, '/log/pycharm/rpc/obtainTicket.action', 'pyc_key')
