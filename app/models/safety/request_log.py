"""
Author: Meng
Date: 2018/8/22
"""
from f_lib.database import Base, SurrogatePK
from app.extension import db
from datetime import datetime


class RequestLog(Base, SurrogatePK):
    __tablename__ = 's_req_log'
    id = db.Column(db.INTEGER, primary_key=True)
    ip = db.Column(db.String(100))
    path = db.Column(db.String(200))
    username = db.Column(db.String(100))
    agent = db.Column(db.String(500))
    get_args = db.Column(db.TEXT)
    post_args = db.Column(db.TEXT)
    method = db.Column(db.String(100))
    url = db.Column(db.TEXT)
    time = db.Column(db.DATETIME)
    create_time = db.Column(db.DATETIME)

    def __init__(self, ip, **kwargs):
        db.Model.__init__(self, ip=ip, **kwargs)
        now = datetime.now()
        self.create_time = now.strftime('%Y-%m-%d %H:%M:%S')
        self.time = now.strftime('%Y-%m-%d %H')
