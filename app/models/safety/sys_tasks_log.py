"""
Author: Meng
Date: 2018/8/26
"""
from f_lib.database import Base, SurrogatePK
from sqlalchemy import Column, String, Integer, BOOLEAN, DATETIME

from app.extension import db

from datetime import datetime


class SysTasksLog(Base, SurrogatePK):
    __tablename__ = 'f_sys_task_log'

    id = Column(Integer, primary_key=True)
    task_name = Column(String(200))
    level = Column(String(100), default='info')
    content = Column(db.TEXT)
    time = Column(db.DATETIME, default=datetime.now())

    def __init__(self, task_name, content, **kwargs):
        db.Model.__init__(self, task_name=task_name, content=content, **kwargs)
