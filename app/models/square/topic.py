"""
Author: Meng
Date: 2018/11/4
"""
from f_lib.database import SurrogatePK, Base
from app.extension import db
from f_lib.hash import hash_sha1
from datetime import datetime
from sqlalchemy.dialects.mysql import MEDIUMTEXT


class Topics(Base, SurrogatePK):
    __tablename__ = 'f_topic'
    id = db.Column(db.INTEGER, primary_key=True)
    user_id = db.Column(db.Integer)
    name = db.Column(db.String(200))
    title = db.Column(db.String(200))
    content = db.Column(MEDIUMTEXT)
    is_show = db.Column(db.Integer, default=1)
    is_top = db.Column(db.Integer, default=0)
    like = db.Column(db.Integer)
    dislike = db.Column(db.Integer)
    comment_count = db.Column(db.Integer, default=0)
    ctime = db.Column(db.DATETIME, default=datetime.now())
    utime = db.Column(db.DATETIME)

    def __init__(self, user_id, name, title, **kwargs):
        db.Model.__init__(self, user_id=user_id, name=name, title=title, **kwargs)


class TopicTagsShip(Base, SurrogatePK):
    __tablename__ = 'f_topic_tags_ship'
    id = db.Column(db.INTEGER, primary_key=True)
    topic_id = db.Column(db.Integer)
    tag_id = db.Column(db.Integer)
    ctime = db.Column(db.DATETIME, default=datetime.now())

    def __init__(self, topic_id, tag_id, **kwargs):
        db.Model.__init__(self, topic_id=topic_id, tag_id=tag_id, **kwargs)


class TopicTags(Base, SurrogatePK):
    __tablename__ = 'f_topic_tags'
    id = db.Column(db.INTEGER, primary_key=True)
    tag_name = db.Column(db.String(100))
    from_uid = db.Column(db.Integer)
    like = db.Column(db.Integer, default=0)
    dislike = db.Column(db.Integer, default=0)
    is_default = db.Column(db.Integer)
    ctime = db.Column(db.DATETIME, default=datetime.now())

    def __init__(self, tag_name, **kwargs):
        db.Model.__init__(self, tag_name=tag_name, **kwargs)


class TopicFileShip(Base, SurrogatePK):
    __tablename__ = 'f_topic_file_ship'
    id = db.Column(db.INTEGER, primary_key=True)
    topic_id = db.Column(db.Integer)
    filename = db.Column(db.String(200))
    name = db.Column(db.String(200))
    is_img = db.Column(db.Integer)
    time = db.Column(db.DATETIME, default=datetime.now())

    def __init__(self, topic_id, filename, **kwargs):
        db.Model.__init__(self, topic_id=topic_id, filename=filename, **kwargs)


class TopicComment(Base, SurrogatePK):
    __tablename__ = 'f_topic_comment'
    id = db.Column(db.INTEGER, primary_key=True)
    idx = db.Column(db.Integer)
    is_top = db.Column(db.Integer, default=0)
    topic_id = db.Column(db.Integer)
    from_uid = db.Column(db.Integer)
    from_name = db.Column(db.String(200))
    content = db.Column(db.TEXT)
    reply_count = db.Column(db.Integer, default=0)
    like = db.Column(db.Integer, default=0)
    dislike = db.Column(db.Integer, default=0)
    is_show = db.Column(db.Integer, default=1)
    time = db.Column(db.DATETIME, default=datetime.now())

    def __init__(self, topic_id, from_uid, from_name, content, **kwargs):
        db.Model.__init__(self, topic_id=topic_id, from_uid=from_uid, from_name=from_name, content=content, **kwargs)


"""
reply_type： 表示回复的类型,因为回复可以是针对评论的回复(comment)，
也可以是针对回复的回复(reply)，通过这个字段来区分两种情景。
 
reply_id： 表示回复目标的id，如果reply_type是comment的话，
那么reply_id＝commit_id，如果reply_type是reply的话，这表示这条回复的父回复。
"""


class CommentReply(Base, SurrogatePK):
    __tablename__ = 'f_comment_reply'
    id = db.Column(db.INTEGER, primary_key=True)
    idx = db.Column(db.Integer)
    comment_id = db.Column(db.Integer)
    reply_id = db.Column(db.Integer)
    reply_idx = db.Column(db.Integer)
    reply_type = db.Column(db.String(100))
    content = db.Column(db.TEXT)
    from_uid = db.Column(db.Integer)
    from_name = db.Column(db.String(200))
    to_uid = db.Column(db.Integer)
    like = db.Column(db.Integer, default=0)
    dislike = db.Column(db.Integer, default=0)
    is_show = db.Column(db.Integer, default=1)
    ctime = db.Column(db.DATETIME, default=datetime.now())

    def __init__(self, comment_id, reply_id, reply_type, **kwargs):
        db.Model.__init__(self, comment_id=comment_id, reply_id=reply_id, reply_type=reply_type, **kwargs)
