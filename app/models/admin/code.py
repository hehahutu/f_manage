"""
Author: Meng
Date: 2018/7/29
"""
from app.extension import db
from f_lib.database import Base, SurrogatePK
from datetime import datetime
from random import Random


class LCode(SurrogatePK, Base):
    __tablename__ = "s_code"
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200))
    user_id = db.Column(db.Integer)
    username = db.Column(db.String(500))
    time = db.Column(db.DATETIME, default=datetime.now())

    def __init__(self, user_id, username):
        db.Model.__init__(self, user_id=user_id, username=username)
        self.set_code()

    def set_code(self):
        ra = Random()
        ch = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789'
        self.code = ''.join([ra.choice(ch) for i in range(6)])