"""
CREATE: 2018/5/29
AUTHOR:　HEHAHUTU
"""
from f_lib.database import Base, SurrogatePK
from sqlalchemy_utils.types.choice import ChoiceType
from app.extension import db
from .user import ROLES, NORMAL
from flask_restful import reqparse
from datetime import datetime
import os

"""
share_expired: 分享key失效时间单位天
"""


class FConfig(Base, SurrogatePK):
    __tablename__ = 'f_config'

    id = db.Column(db.Integer, primary_key=True)
    server_name = db.Column(db.String(100))
    is_register = db.Column(db.Integer)
    default_reg_role = db.Column(ChoiceType(ROLES), default=NORMAL)
    backup_http = db.Column(db.String(20), default='http:')
    backup_ip = db.Column(db.String(100))
    backup_port = db.Column(db.Integer)
    backup_path = db.Column(db.String(200))
    backup_base_path = db.Column(db.String(500))
    update_time = db.Column(db.DATETIME)
    create_time = db.Column(db.DATETIME, default=datetime.now())
    share_expired = db.Column(db.Integer, default=60)
    share_always = db.Column(db.Integer, default=0)
    share_host = db.Column(db.String(200))
    avatar_size = db.Column(db.Integer, default=2048)
    email_info = db.Column(db.String(1000))
    host = db.Column(db.String(200))
    app_secret = db.Column(db.String(100))

    def __init__(self, server_name, is_register, **kwargs):
        db.Model.__init__(self, server_name=server_name, is_register=is_register, **kwargs)

    def __repr__(self):
        return f"""
        config register: {self.is_register}\n 
        default reg role: {self.default_reg_role}\n
        backup ip: {self.backup_ip}:{self.backup_port}
        """

    def set_backup_base_path(self):
        if not os.path.exists(self.backup_base_path):
            try:
                os.mkdir(self.backup_base_path)
            except:
                raise IOError(f'mkdir {self.backup_base_path} failed')


config_parse = reqparse.RequestParser()
config_parse.add_argument('server_name', type=str, required=True)
config_parse.add_argument('is_register', type=int, required=True)
config_parse.add_argument('default_reg_role', type=str, choices=[key[0] for key in ROLES])
config_parse.add_argument('backup_ip', type=str)
config_parse.add_argument('backup_port', type=str)
config_parse.add_argument('backup_base_path', type=str)
config_parse.add_argument('share_expired', type=str)
config_parse.add_argument('share_host', type=str)
config_parse.add_argument('avatar_size', type=str)
config_parse.add_argument('host', type=str)


class Notice(Base, SurrogatePK):
    __tablename__ = 'f_config_notice'

    id = db.Column(db.Integer, primary_key=True)
    notice_type = db.Column(db.String(100))
    show = db.Column(db.Boolean, default=True)
    show_close = db.Column(db.Boolean, default=True)
    text = db.Column(db.String(200))
    msg = db.Column(db.String(200))
    url = db.Column(db.String(200))
    enable = db.Column(db.Integer)
    ctime = db.Column(db.DATETIME, default=datetime.now())

    def __init__(self, notice_type, text, **kwargs):
        db.Model.__init__(self, notice_type=notice_type, text=text, **kwargs)
