"""
Author: Meng
Date: 2018/10/7
"""

from f_lib.database import Base, SurrogatePK
from sqlalchemy import Column, String, Integer, BOOLEAN, DATETIME
from sqlalchemy_utils.types.choice import ChoiceType
from app.extension import db
import uuid
from datetime import datetime


class MiTask(Base, SurrogatePK):
    __tablename__ = 'f_mi_task'
    id = Column(Integer, primary_key=True)
    task_name = Column(String(200), nullable=False)
    run_time = Column(db.Integer)
    cookies = Column(db.TEXT)

    def __init__(self, task_name, cookies, run_time):
        db.Model.__init__(self, task_name=task_name, cookies=cookies, run_time=run_time)
