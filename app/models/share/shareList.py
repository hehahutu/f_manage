"""
Author: Meng
Date: 2018/7/11
"""
from f_lib.database import SurrogatePK, Base
from app.extension import db
from f_lib.hash import hash_sha1
from datetime import datetime
"""
is_always: 1 表示永久有效
expired： 过期时间，与is_always互斥
"""


class ShareKey(Base, SurrogatePK):
    __tablename__ = 'f_share_key'
    id = db.Column(db.INTEGER, primary_key=True)
    share_key = db.Column(db.String(500))
    user_id = db.Column(db.Integer)
    share_user = db.Column(db.String(500))
    time = db.Column(db.DATETIME, default=datetime.now())
    is_always = db.Column(db.Integer)
    expired = db.Column(db.DATETIME)

    def __init__(self, user_id, share_user, is_always, expired, **kwargs):
        db.Model.__init__(self, user_id=user_id, share_user=share_user, is_always=is_always, expired=expired,
                          **kwargs)
        self.set_share_key()

    def set_share_key(self):
        self.share_key = hash_sha1(datetime.now())


class ShareShip(Base, SurrogatePK):
    __tablename__ = 'f_share_ship'
    id = db.Column(db.INTEGER, primary_key=True)
    share_key = db.Column(db.String(500))
    user_id = db.Column(db.Integer)
    type = db.Column(db.String(500))
    type_id = db.Column(db.Integer)
    time = db.Column(db.DATETIME, default=datetime.now())

    def __init__(self, share_key, type, **kwargs):
        db.Model.__init__(self, share_key=share_key, type=type, **kwargs)