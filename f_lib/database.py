"""
CREATE: 2018/5/12
AUTHOR:　HEHAHUTU
"""
import functools
from flask import request, url_for
from sqlalchemy.orm import relationship
from app.extension import db
from .compat import basestring

# Alias common SQLAlchemy names
relationship = relationship


class CRUDMixin(object):
    """Mixin that adds convenience methods for CRUD (create, read, update, delete)
    operations.
    """

    @classmethod
    def create(cls, **kwargs):
        """Create a new record and save it the database."""
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=True, **kwargs):
        """Update specific fields of a record."""
        # Prevent changing ID of object
        kwargs.pop('id', None)
        for attr, value in kwargs.items():
            # Flask-RESTful makes everything None by default :/
            if value is not None:
                setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        """Save the record."""
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        """Remove the record from the database."""
        db.session.delete(self)
        return commit and db.session.commit()


class Base(CRUDMixin, db.Model):
    """Base model class that includes CRUD convenience methods."""
    __abstract__ = True


# From Mike Bayer's "Building the app" talk
# https://speakerdeck.com/zzzeek/building-the-app
class SurrogatePK(object):
    """A mixin that adds a surrogate integer 'primary key' column named
    ``id`` to any declarative-mapped class.
    """
    __table_args__ = {'extend_existing': True}

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def get_by_id(cls, id):
        try:
            id = int(id)
        except:
            raise TypeError('id must int')
        if id <= 0:
            raise ValueError('ID must not be negative or zero!')
        if any(
                (isinstance(id, basestring) and id.isdigit(),
                 isinstance(id, (int, float))),
        ):
            return cls.query.get(int(id))
        return None

    @classmethod
    def query_one(cls, **kwargs):
        if kwargs:
            return cls.query.filter_by(**kwargs).first()

        return cls.query.filter().first() or None

    @classmethod
    def query_many(cls, **kwargs):
        return cls.query.filter_by(**kwargs).all()

    @classmethod
    def query_count(cls, **kwargs):
        return cls.query.filter_by(**kwargs).count()

    @classmethod
    def delete_by(cls, **kwargs):
        keys = list(kwargs.keys())
        if len(keys) > 1:
            where_info = ' and '.join([f'{key} = {kwargs.get(key)}' for key in keys])
        else:
            where_info = f'{keys[0]} = {kwargs[keys[0]]}'
        sql = """delete from {} where {}""".format(cls.__tablename__, where_info)
        db.session.execute(sql)
        db.session.commit()

    @classmethod
    def query_group_path_files(cls, path=None, user_id=None):
        if path:
            sql = """select * from f_file where folder_path like '{}%' and user_id = {} and is_trash != 1""".format(
                path, user_id)
            # print(sql)
            data = db.session.execute(sql)
            if data:
                return data
        return None

    @classmethod
    def query_search_file_like(cls, keyword, user_id, check):
        sql = """select {} from f_file where name like '%{}%' and user_id = {} and is_trash != 1""".format(
            ','.join(check), keyword, user_id)
        data = db.session.execute(sql)
        if data:
            return data
        return None

    @classmethod
    def query_where_many(cls, params: list, wheres: list, limit: list = [], order=['id', 'desc']):
        params = ["`{}`".format(item) for item in params]
        if len(limit) > 0:
            sql = """select {} from {} where {} order by {} {}""".format(
                ",".join(params),
                cls.__tablename__,
                ' and '.join(wheres),
                "{} {}".format(order[0], order[1]),
                'limit {},{}'.format(limit[0], limit[1])
            )

        else:
            sql = """select {} from {} where {} order by {}""".format(
                ','.join(params),
                cls.__tablename__,
                ' and '.join(wheres),
                "{} {}".format(order[0], order[1]),

            )

        data = db.session.execute(sql)
        if data:
            return data
        return None

    @classmethod
    def query_where_many_count(cls, wheres: list):
        sql = """select count(id) from {} where {}""".format(
            cls.__tablename__, ' and '.join(wheres))
        data = db.session.execute(sql)
        if data:
            return list(data)[0][0]
        return 0


def ReferenceCol(tablename, nullable=False, pk_name='id', **kwargs):
    """Column that adds primary key foreign key reference.

    Usage: ::

        category_id = ReferenceCol('category')
        category = relationship('Category', backref='categories')
    """
    return db.Column(
        db.ForeignKey("{0}.{1}".format(tablename, pk_name)),
        nullable=nullable, **kwargs)  # pragma: no cover


def paginate(max_per_page=100):
    def decorator(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            page = request.args.get('page', 1, type=int)
            per_page = min(request.args.get('per_page', max_per_page,
                                            type=int),
                           max_per_page)

            query = func(*args, **kwargs)
            p = query.paginate(page, per_page)

            meta = {
                'page': page,
                'per_page': per_page,
                'total': p.total,
                'pages': p.pages,
            }

            links = {}
            if p.has_next:
                links['next'] = url_for(request.endpoint, page=p.next_num,
                                        per_page=per_page, **kwargs)
            if p.has_prev:
                links['prev'] = url_for(request.endpoint, page=p.prev_num,
                                        per_page=per_page, **kwargs)
            links['first'] = url_for(request.endpoint, page=1,
                                     per_page=per_page, **kwargs)
            links['last'] = url_for(request.endpoint, page=p.pages,
                                    per_page=per_page, **kwargs)

            meta['links'] = links
            result = {
                'items': p.items,
                'meta': meta
            }

            return result, 200

        return wrapped

    return decorator
