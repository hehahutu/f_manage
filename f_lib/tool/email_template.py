"""
Author: Meng
Date: 2018/7/29
"""

def code_template(code):
    email_msg = """
                             <div>
                              <p>Hello：</p>
                              <p>你收到这封邮件是因为在云盘操作二次验证，授权码：<strong>{}</strong>
                               ，如果您没有操作过，请忽略邮件~</p>
                            </div>
                            """.format(code)

    return email_msg