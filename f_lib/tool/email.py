"""
Author: Meng
Date: 2018/7/23
"""
import smtplib
from email.mime.text import MIMEText
from email.header import Header
import os


def send_email_html(admin, title, receiver, mail_msg):
    ad = admin.split(';')
    sender = ad[0]
    MAIL_USER = ad[1]
    MAIL_PASS = ad[2]
    MAIL_HOST = ad[3]
    MAIL_SMTP_PORT = ad[4]
    sender = sender
    receivers = [receiver, ]  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

    # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
    message = MIMEText(mail_msg, 'html', 'utf8')

    message['From'] = Header("云盘-惊蛰", 'utf8')
    message['To'] = Header(receiver, 'utf8')

    subject = ''
    message['Subject'] = Header(title, 'utf8')
    try:
        smtpObj = smtplib.SMTP_SSL()
        smtpObj.connect(MAIL_HOST, MAIL_SMTP_PORT)

        smtpObj.login(MAIL_USER, MAIL_PASS)

        smtpObj.sendmail(sender, receivers, message.as_string())

        return True
    except smtplib.SMTPException as f:
        print(f)

        return False


def send_email_html_executor(admin, title, receiver, mail_msg):
    from main import app
    with app.app_context():
        ad = admin.split(';')
        sender = ad[0]
        MAIL_USER = ad[1]
        MAIL_PASS = ad[2]
        MAIL_HOST = ad[3]
        MAIL_SMTP_PORT = ad[4]
        sender = sender
        receivers = [receiver, ]  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

        # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
        message = MIMEText(mail_msg, 'html', 'utf8')

        message['From'] = Header("云盘-惊蛰", 'utf8')
        message['To'] = Header(receiver, 'utf8')

        subject = ''
        message['Subject'] = Header(title, 'utf8')
        try:
            smtpObj = smtplib.SMTP_SSL()
            smtpObj.connect(MAIL_HOST, MAIL_SMTP_PORT)

            smtpObj.login(MAIL_USER, MAIL_PASS)

            smtpObj.sendmail(sender, receivers, message.as_string())

            return True
        except smtplib.SMTPException as f:
            return False
