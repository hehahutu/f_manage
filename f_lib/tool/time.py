"""
Author: Meng
Date: 2018/7/25
"""
from datetime import datetime, timedelta


def strf_time(d: datetime, ft='%Y-%m-%d %H:%M:%S'):
    return d.strftime(ft)


def utc_format(utc: str):
    try:
        tis = datetime.strptime(utc, "%Y-%m-%dT%H:%M:%S.%fZ")
        local_time = tis + timedelta(hours=8)
        return strf_time(local_time)
    except:
        return utc