"""
Author: Meng
Date: 2018/8/25
"""
from PIL import Image
import os
from settings import THUMB_IMG


def thumb_image(path, filename, size=(60, 60)):
    try:
        f, t = os.path.splitext(filename)
        if t.lower() in THUMB_IMG:
            thumb_filename = f + '_{}_{}'.format(size[0], size[1]) + t
            thumb_path = os.path.join(path, thumb_filename)
            img = Image.open(os.path.join(path, filename))
            thumb = img.resize(size, Image.ANTIALIAS)
            thumb.save(thumb_path, quality=70)
            return True
        return None
    except Exception as e:
        print(e)
        return None
