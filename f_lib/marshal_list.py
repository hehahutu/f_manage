"""
Author: Meng
Date: 2018/7/1
"""
from datetime import datetime
import sqlalchemy_utils.types.choice
from sqlalchemy.engine.result import ResultProxy


def marshal(params: dict, data):
    if isinstance(data, list) or isinstance(data, ResultProxy):
        if data:
            new_data = []
            for da in data:
                try:
                    for key in params.keys():
                        val = getattr(da, key)
                        if isinstance(val, datetime):
                            val = str(val)

                        if isinstance(val, sqlalchemy_utils.types.choice.Choice):
                            val = val.code
                        params[key] = val
                    new_data.append(params.copy())
                except:
                    pass
            return new_data

        return []
    else:
        for key in params.keys():
            val = getattr(data, key)
            if isinstance(val, datetime):
                val = str(val)
            if isinstance(val, sqlalchemy_utils.types.choice.Choice):
                val = val.code
            params[key] = val
        return params.copy()

