class Code:
    SUCCESS = 0
    FAILED = 2
    NO_PARAM = 400

    # login code
    AUTHORIZED_FAILED = 100
    USERNAME_EMPTY = 101
    PASSWORD_ERROR = 102

    msg = {
        SUCCESS: "success",
        FAILED: 'request failed',
        NO_PARAM: "invalid param",
        USERNAME_EMPTY: 'username empty',
        PASSWORD_ERROR: 'password error',
        AUTHORIZED_FAILED: '账号信息过期，请重新登录'

    }


def to_json(**kwargs):
    data = {}
    for key, value in kwargs.items():
        data[key] = value
    return data


class Msg:
    @staticmethod
    def success(msg='ok', **kwargs):
        if kwargs:
            return {'code': Code.SUCCESS, 'message': msg, 'data': to_json(**kwargs)}
        else:
            return {'code': Code.SUCCESS, 'message': msg}

    @staticmethod
    def failed(msg, **kwargs):
        if kwargs:
            return {'code': Code.FAILED, 'message': msg, 'data': to_json(**kwargs)}
        else:
            return {'code': Code.FAILED, 'message': msg}

    @staticmethod
    def failed_dict(code: int, **kwargs):
        if kwargs:
            return {'code': code, 'message': code_dict.get(str(code), 'invalid code'), 'data': to_json(**kwargs)}
        else:
            return {'code': code, 'message': code_dict.get(str(code), 'invalid code')}

    @staticmethod
    def login_username_error(msg):
        return {'code': Code.USERNAME_EMPTY, 'message': msg}

    @staticmethod
    def login_password_error(msg):
        return {'code': Code.PASSWORD_ERROR, 'message': msg}

    @staticmethod
    def authorized_failed(code=Code.AUTHORIZED_FAILED, **kwargs):
        return {'code': code, 'message': Code.msg[code], 'data': to_json(**kwargs)}


code_dict = {
    '1001': '磁盘空间不足，无法上传',
    '1002': '无效的id',
    '1003': '无效的参数',
    '1004': '抱歉该分享链接已失效',
    '2001': "need admin role auth",
    '2002': "need group role auth",
    '2003': "邮箱不存在",
    '2004': "用户名不存在",
    '2005': '注册通道暂时关闭，无法注册',

    '3000': '未检索到任务！',
    '3001': '任务已在运行状态！',

    '400': 'invalid param',
    '401': '授权码错误',
    '402': '由于您一段时间内访问频率太高，已被系统拒绝访问，请稍后再试',
    '403': '无效的secret',
    '5000': '文件不存在或无效的文件名',
    '5001': '同级目录不允许同名文件夹',
    '5002': '该文件夹下没有文件无需下载',

    '6000': '向目标邮箱发送确认邮件失败',
    '7000': '只能删除自己的评论~',
    '7001': '只能删除自己的动态~',



}

