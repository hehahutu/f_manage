"""
Author: Meng
Date: 2018/8/26
"""

from .clear_download_zip import _clear_task_name, clear_zips
from .clear_trash_file import _clear_trash_file_name, clear_files

all_tasks = {
    _clear_task_name: clear_zips,
    _clear_trash_file_name: clear_files
}