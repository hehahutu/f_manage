"""
CREATE: 2018/5/13
AUTHOR:　HEHAHUTU
"""
import hashlib


def hash_md5(value):
    value = str(value)
    h = hashlib.md5()
    h.update(value.encode('utf8'))
    return h.hexdigest().upper()


def hash_sha256(value):
    value = str(value)
    h = hashlib.sha256()
    h.update(value.encode('utf8'))
    return h.hexdigest().upper()


def hash_sha1(value):
    value = str(value)
    h = hashlib.sha1()
    h.update(value.encode('utf8'))
    return h.hexdigest().upper()
# if __name__ == '__main__':
#     print(hash_data('aaa'))
