"""
Author: Meng
Date: 2019/4/21
"""
import requests

ip_list = """111.177.166.220	9999	高匿名	HTTP	湖北省随州市 电信	2秒	2019-04-21 21:30:59
110.52.235.254	9999	高匿名	HTTP	湖南省岳阳市 联通	2秒	2019-04-21 20:31:00
47.93.18.195	80	高匿名	HTTP	北京市北京市 阿里云	0.7秒	2019-04-21 19:30:59
119.180.178.231	8060	高匿名	HTTP	山东省威海市 联通	2秒	2019-04-21 18:31:01
60.13.42.233	9999	高匿名	HTTP	甘肃省平凉市 联通	0.9秒	2019-04-21 17:30:55
125.126.193.164	9999	高匿名	HTTP	浙江省台州市 电信	1秒	2019-04-21 16:31:01
49.86.181.205	9999	高匿名	HTTP	江苏省扬州市 电信	1秒	2019-04-21 15:31:01
116.196.121.56	8080	高匿名	HTTP	北京市北京市 京东云 BGP多线	2秒	2019-04-21 14:31:01
114.113.222.132	80	高匿名	HTTP	北京市北京市 联通	0.8秒	2019-04-21 13:30:59
122.243.13.161	9000	高匿名	HTTP	浙江省金华市 电信	1秒	2019-04-21 12:30:55
106.15.42.179	33543	高匿名	HTTP	上海市上海市 阿里云计算有限公司 阿里云	1秒	2019-04-21 11:31:01
121.233.206.211	9999	高匿名	HTTP	江苏省扬州市 电信	2秒	2019-04-21 10:31:01
117.91.251.46	9999	高匿名	HTTP	江苏省扬州市 电信	3秒	2019-04-21 09:31:01
125.126.194.66	9999	高匿名	HTTP	浙江省台州市 电信	2秒	2019-04-21 08:30:59
111.75.223.9	30646	高匿名	HTTP	江西省南昌市 电信	3秒	2019-04-21 07:31:01
"""

import time
import random


def get_proxy_ips():
    ip_li = ip_list.split('\n')
    ip_li = ['http://{}'.format(item.split(' ')[0]) for item in ip_li]
    ip_li = [item.split('\t')for item in ip_li]
    ip_li.pop(-1)
    ip_li = ['{}:{}'.format(item[0], item[1]) for item in ip_li]
    print(ip_li)
    for idx, item in enumerate(ip_li):
        print(idx)
        try:
            time.sleep(1)
            html = requests.get('http://www.baidu.com', proxies={'http': item}, timeout=5)
        except Exception as e:
            print(e)
            ip_li.remove(item)
    print(ip_li)
    return ip_li


def ips():
    url = ['http://125.72.70.46:8060', 'http://47.98.237.129:80', 'http://171.83.164.225:9999', 'http://117.191.11.111:8080', 'http://106.14.172.210:8080', 'http://117.191.11.77:80', 'http://120.210.219.74:8080', 'http://60.217.142.117:8060', 'http://118.24.182.249:8081', 'http://117.158.189.238:9999', 'http://111.206.6.101:80', 'http://59.34.2.92:3128', 'http://221.2.155.35:8060', 'http://60.186.77.89:9999', 'http://42.51.42.201:808', 'http://116.209.55.16:9999', 'http://221.2.174.3:8060', 'http://218.59.139.238:80', 'http://202.112.237.102:3128', 'http://47.97.190.145:9999', 'http://119.190.136.247:8060', 'http://119.179.135.181:8060', 'http://183.196.97.125:46006', 'http://121.40.138.161:8000', 'http://111.177.188.93:9999', 'http://113.121.241.128:61234', 'http://120.237.156.43:8088', 'http://119.190.185.35:8060', 'http://36.7.89.233:8060', 'http://106.2.238.2:3128', 'http://119.179.130.59:8060', 'http://119.190.34.70:80', 'http://116.228.233.90:8082', 'http://49.51.68.122:1080', 'http://180.76.150.182:9000', 'http://116.209.52.197:9999', 'http://47.96.122.110:8080', 'http://124.202.166.171:82', 'http://61.176.223.7:58822', 'http://121.12.85.2:80', 'http://121.8.98.196:80', 'http://120.210.219.105:80', 'http://221.6.32.206:41816', 'http://202.100.83.139:80', 'http://119.191.79.46:80', 'http://116.209.55.92:9999', 'http://120.210.219.104:80', 'http://118.24.246.249:80', 'http://121.8.98.197:80', 'http://221.2.174.99:8060', 'http://117.135.77.30:8060', 'http://101.231.104.82:80', 'http://125.62.40.3:8080', 'http://119.179.136.73:8060', 'http://220.175.133.47:8060', 'http://115.159.31.195:8080', 'http://103.38.178.114:55502', 'http://203.156.209.121:8080', 'http://101.37.20.241:443', 'http://111.231.87.160:8088', 'http://120.194.18.90:81', 'http://221.2.175.238:8060', 'http://114.95.165.115:8060', 'http://27.208.90.235:8060', 'http://14.204.211.7:8060', 'http://123.207.68.166:1080', 'http://119.179.172.125:8060', 'http://58.240.53.194:8080', 'http://120.79.99.27:80', 'http://119.180.178.231:8060', 'http://118.113.14.123:8080', 'http://58.240.53.197:80', 'http://218.57.146.212:8888', 'http://114.245.196.234:8060']
    idx = random.randint(0, len(url)-1)
    return url[idx]


if __name__ == '__main__':
    get_proxy_ips()