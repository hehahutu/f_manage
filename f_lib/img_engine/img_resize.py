"""
Author: Meng
Date: 2019/4/20
"""
import os
from PIL import Image

# import pyautogui
# import re
'''
把当前目录下的10*10张jpeg格式图片拼接成一张大图片

'''

obj_path = r'C:\userFiles\一人之下_mc'
save_base_path = r'C:\userFiles\一人之下_rp'

all_p_path = os.listdir(obj_path)
all_p_path = [(os.path.join(obj_path, name), name) for name in all_p_path]

all_img_path = [([os.path.join(pa[0], name) for name in os.listdir(pa[0])], pa[1]) for pa in all_p_path]


def resize_img(p):
    all_path = p[0]
    if len(all_path) > 0:
        save_path = os.path.join(save_base_path, '{}.png'.format(p[1]))
        # 图片压缩后的大小
        width_i = 800
        height_i = 1271

        # 每行每列显示图片数量
        line_max = 1
        row_max = len(all_path)
        toImage = Image.new('RGBA', (width_i * line_max, height_i * row_max))
        all_path.sort()

        for i in range(0, row_max):
            try:

                pic_fole_head = Image.open(all_path[i])

                width, height = pic_fole_head.size
                #
                tmppic = pic_fole_head.resize((width_i, height))
                #
                loc = (0, i * height + 2)

                # print("第" + str(num) + "存放位置" + str(loc))
                toImage.paste(tmppic, loc)
            except:
                print('*'*100, all_path[i])

        print(toImage.size)
        print(save_path)
        toImage.save(save_path)
    else:
        print(p)


if __name__ == '__main__':
    print(all_img_path)
    allpath = all_img_path
    allpath = allpath[66:]
    for idx, item in enumerate(allpath):
        print(idx)
        print()
        resize_img(item)
