"""
Author: Meng
Date: 2019/4/21
"""
import requests
from bs4 import BeautifulSoup
import os
import base64
import time
import random
from f_lib.img_engine.proxy_ip import get_proxy_ips, ips


headers = {
    'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Mobile Safari/537.36'
}
base_url = 'http://m.taduo.net'
base_path = r'C:\userFiles\一人之下_mc'


def get_all_title_url():
    url = 'http://m.taduo.net/manhua/2/'
    html = requests.get(url, headers=headers)
    html = BeautifulSoup(html.content.decode('gbk'), 'html.parser')
    # print(html)
    p_obj = html.find('div', {'id': 'chapterList2'}).find('ul')
    p_list = p_obj.find_all('li')
    print(p_list)
    url_list = []
    for item in p_list:
        a = item.find('a')
        url_list.append([a.get('title'), '{}{}'.format(base_url, a.get('href'))])
    print(url_list)
    return url_list


def get_p_img_url(p_info):
    url = p_info[1]
    title = p_info[0]
    # print(p_info)

    html = requests.get(url, headers=headers)
    html = BeautifulSoup(html.content.decode('gbk'), 'html.parser')
    # print(html)
    all_page = html.find_all('script')[3]
    all_page = all_page.text
    all_page = all_page.strip().split('\r\n')
    c = all_page[2]
    c.strip()
    c = c.split('\"')[1]
    v = base64.b64decode(c)
    v = v.decode()
    v = v.split('(')[-2]
    # v.split('.split', '')
    z = v.split(']')
    keys = z[0].split('[')[-1].replace('\\', '').replace('\'', '').split(',')
    keys = [key.split('/') for key in keys]
    # print(keys)
    value = z[1].split(',')[-1].split('.')[0].replace('\'', '').split('|')
    new_keys = []
    for key in keys:
        la = key[-1]
        print(key)
        is_l = False
        if len(key) > 6:
            is_l = True
            key = key[:6]
        # print(key)
        a = key.pop(-1)

        key = [int(k) for k in key]
        c, d = a.split('.')

        c = int(c, 36)
        key.append(c)
        key.append(int(d))
        if is_l:
            key.append(la)
        new_keys.append(key)

    urls = []
    if title != '通知':
        for key in new_keys:
            a = value[key[0]]
            path = '{}/{}/{}/{}/{}.{}'.format(value[key[0]],
                                              value[key[1]],
                                              value[key[2]],
                                              value[key[3]],
                                              value[key[4]],
                                              value[key[5]],
                                              )
            h1 = 'http://fn.taduo.net/'
            h = 'http://f.taduo.net/'
            if a == '2019' or a == '2017':
                h = h1
            url = h + path
            if len(key) > 6:
                url = '{}/{}'.format(url, key[-1])
            urls.append(url)
    return [title, urls]


def save_p_img(p_info):
    title = str(p_info[0]).replace('.', '').replace('?', '')
    url_list = p_info[1]
    save_path = os.path.join(base_path, title)
    if os.path.exists(save_path) is False:
        os.mkdir(save_path)
    for idx, url in enumerate(url_list):
        print(url)
        ip = ips()
        print(ip)
        try:
            html = requests.get(url, headers=headers, timeout=20)
        except:
            time.sleep(5)
            print('html failed try')
            html = requests.get(url, headers=headers, timeout=20)
        path = os.path.join(save_path, '{}.jpg'.format(idx))
        with open(path, 'wb') as f:
            f.write(html.content)
        print('save success {}'.format(path))
        sec = random.uniform(0.3, 2.8)
        print('sleep {}s'.format(sec))
        time.sleep(sec)


if __name__ == '__main__':
    alls = get_all_title_url()
    alls = alls[89:]
    for idx, urls in enumerate(alls):
        print(urls, idx)
        urls = get_p_img_url(urls)
        print(urls)
        save_p_img(urls)
    # get_proxy_ip()
